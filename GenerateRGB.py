from bs4 import BeautifulSoup

fr = open('color.html')
soup = BeautifulSoup(fr,"html.parser")
RGBs = []
for tr in soup.find_all('tr'):
    childrenlist = []

    for td in tr.find_all('td'):
        childrenlist.append(td.string)
    RGBs.append([int(childrenlist[4]),int(childrenlist[5]),int(childrenlist[6])])

fw = open('colormap.txt','w')
for RGB in RGBs:
    fw.write('%d %d %d\n'%(RGB[0],RGB[1],RGB[2]))

fw.close()
fr.close()
