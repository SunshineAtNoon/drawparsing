from PIL import Image
import numpy as np

import argparse
parser = argparse.ArgumentParser(description='This is python program that imposes segmentations onto images')

parser.add_argument('-o', action='store',help='Original Image Path')
parser.add_argument('-s', action='store',help='Segmentation Label Image Path')

results = parser.parse_args()

object_number = 396 #there's total 395 objects in SUN and SPC, 0 is for other object in SPC
origin = Image.open(str(results.o))
seg = Image.open(str(results.s))
width,height = seg.size

#color dictionary
fcolor = open('colormap.txt')
fobject  = open('object.txt')


color_list = []
colors = fcolor.readlines()
for i in range(0,object_number):#we need this much color
    RGB = colors[i].split(' ')
    RGB_list = [int(RGB[0]),int(RGB[1]),int(RGB[2])]
    color_list.append(RGB_list)
color_map = np.transpose(np.asarray(color_list))
seg_array = np.asarray(seg)

seg_array = seg_array.reshape([1,width*height])

eye_matrix = np.eye(object_number)

onehot = eye_matrix[seg_array]
onehot = onehot.reshape(width*height,object_number)
onehot = onehot.transpose()

color_img = np.dot(color_map,onehot) #3*396, 396*(width*height)

color_img = color_img.reshape(3,height,width)
color_img = np.rollaxis(color_img, 2)
color_img = np.rollaxis(color_img, 2)
foreground = Image.fromarray(np.uint8(color_img))

foreground = foreground.convert('RGBA')
background = origin.convert('RGBA')
new_img = Image.blend(background,foreground,0.7)
new_img.save('new_img.png','PNG')
