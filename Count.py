from bs4 import BeautifulSoup

fr = open('object_sideindex.html')
soup = BeautifulSoup(fr,"html.parser")

fw = open('nameAndapperances.txt','w')
fw_obj = open('objectlist.txt','w')
for div in soup.find_all('div'):
    object_number = div.contents[-1]
    object_number = int(object_number[2:-1])
    object_name = div.contents[-2].string
    if(object_number >= 100):
        fw.write('%s %d\n'%(object_name,object_number))
        fw_obj.write('%s\n'%(object_name))

fr.close()
fw.close()
