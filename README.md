### To draw scene parse result on an image, just run

```
python GenerateVisualization.py -o [original image path] -s [segmentation image path]
```

- [original image path] is the path of original RGB image
- [segmentation image path] is the path of scene parsing result image, each pixel in the segmentation image indicates the object label of this pixel.

For example:
```
python GenerateVisualization.py -o 2.jpg -s 2.png
```