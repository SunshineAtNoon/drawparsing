fSUN = open('object_Sun.txt')
fSPC = open('object_Bolei.txt')

#generate a dictionary for SPC
dict = {}
lines = fSPC.readlines()
counter = 0
for line in lines:
    counter = counter + 1
    words = line.split()
    for word in words:
        word = word.lower()
        if word not in dict:
            dict[word] = counter

#add SUN into SPC dict
counter = counter + 1
lines = fSUN.readlines()
for line in lines:
    word = line.strip()
    word = word.lower()
    if word not in dict:
        dict[word] = counter
        counter = counter + 1

print('Found %d objects'%(counter))
fdict = open('object.txt','w')
for key, value in dict.iteritems():
    fdict.write('%s %s\n'%(key,value))

fSUN.close()
fSPC.close()
fdict.close()
